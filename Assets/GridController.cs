﻿using UnityEngine;
using System.Collections;

public class GridController : MonoBehaviour {

	private float timeLookedAt;
	private float timeUntilMove;
	public GameObject[] tiles;
	private int winCount;
	public GameObject yup;
	public GameObject background1;
	public GameObject background2;
	public static GridController g;


	public int wins = 0;

	// Use this for initialization
	void Start () {

	}

	public bool winning = false;
	// Update is called once per frame
	void Update () {
			winCount = 0;
			for (int i = 0; i < 8; ++i) {
				if (tiles [i].transform.position.x + 5 == tiles [i + 1].transform.position.x) {
					++winCount;
				} else if (((i == 2 || i == 5) && tiles [i].transform.position.x - 10 == tiles [i + 1].transform.position.x) &&
					tiles [i].transform.position.y - 5 == tiles [i + 1].transform.position.y) {
					++winCount;
				}
			Debug.Log (winCount);

			}
			if (winCount >= 7) {
				winning = true;
				Vector3 winSpot = new Vector3 (tiles [7].transform.position.x + 5, tiles [7].transform.position.y, -5);
				tiles [8].transform.position = (winSpot);
				Invoke ("Win", 3.0f);
			
			}
		
	}
	

	void Win(){
		Debug.Log ("Win");
		for (int j = 0; j < tiles.Length; ++j) {
			tiles [j].transform.position = new Vector3 (1000, 1000, -1000);
			
		}
		background1.transform.position = new Vector3(1000,1000,-1000);
		background2.transform.position = new Vector3(1000,1000,-1000);



	}
}
