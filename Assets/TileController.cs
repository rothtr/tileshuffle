﻿using UnityEngine;
using System.Collections;

public class TileController : MonoBehaviour {

	public GameObject tilePrefab;
	public GameObject backgroundTile2;
	public GameObject backgroundTile;
	private bool right = false;
	private bool left = false;
	private bool up = false;
	private bool down = false;
	public static TileController T;
	// Use this for initialization
	void Start () {
		CreateTiles ();
	}

	void CreateTiles(){

	}
	// Update is called once per frame
	void Update(){

	}
	void MoveAnimate(Vector3 v){

	}

	void OnMouseDown(){
		if (((tilePrefab.transform.position.x + 5).Equals (backgroundTile.transform.position.x)
			&& tilePrefab.transform.position.y.Equals (backgroundTile.transform.position.y))) {
			right = true;
			//move right
			backgroundTile2.transform.Translate (-5, 0, 0);
			tilePrefab.transform.Translate (5, 0, 0);
			backgroundTile.transform.Translate (-5, 0, 0);
			right = false;
		

		} else if (((tilePrefab.transform.position.x - 5).Equals (backgroundTile.transform.position.x)
			&& tilePrefab.transform.position.y.Equals (backgroundTile.transform.position.y))) {
			left = true;
			//move left
			backgroundTile2.transform.Translate (5, 0, 0);
			tilePrefab.transform.Translate (-5, 0, 0);
			backgroundTile.transform.Translate (5, 0, 0);
			left = false;
		

		} else if (((tilePrefab.transform.position.y + 5).Equals (backgroundTile.transform.position.y)
			&& tilePrefab.transform.position.x.Equals (backgroundTile.transform.position.x))) {
			up = true;
			//move up
			backgroundTile2.transform.Translate (0, -5, 0);
			tilePrefab.transform.Translate (0, 5, 0);
			backgroundTile.transform.Translate (0, -5, 0);
			up = false;
	
		} else if (((tilePrefab.transform.position.y - 5).Equals (backgroundTile.transform.position.y)
			&& tilePrefab.transform.position.x.Equals (backgroundTile.transform.position.x))) {
			down = true;
			//move down
			backgroundTile2.transform.Translate (0, 5, 0);
			tilePrefab.transform.Translate (0, -5, 0);
			backgroundTile.transform.Translate (0, 5, 0);
			down = true;
		}
	}
}

